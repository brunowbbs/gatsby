import React from "react"
import Layout from "../components/Layout/index"
import Profile from "../components/Profile"

const AboutPage = () => (
  <>
    <Layout>
      <Profile /> <h1>About Page</h1>
    </Layout>
  </>
)

export default AboutPage
