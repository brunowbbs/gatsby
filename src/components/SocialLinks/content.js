const links = [
  {
    label: "Github",
    url: "https://github.com/brunowbbs",
  },
  {
    label: "Twitter",
    url: "https://twitter.com/brunowbbs",
  },
  {
    label: "Youtube",
    url: "https://youtube.com/brunowbbs",
  },
  {
    label: "Instagram",
    url: "https://instagram.com/brunowbbs",
  },
  {
    label: "Facebook",
    url: "https://facebook.com/wesley.bruno.3192479",
  },
]

export default links
