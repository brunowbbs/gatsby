import style from "styled-components"
import { Link } from "gatsby"

export const ProfileWrapper = style.section`
  color: #8899A6;
  display: flex;  
  flex-direction: column;   
`

export const ProfileLink = style(Link)`
  color: #8899A6;
  text-decoration: none;
  transition: color 0.5s;
  
  &:hover{
    color: #1FA1F2;
  }
`

export const ProfileAuthor = style.h1`
  font-size: 1.6rem;
  margin: 0.5rem auto 1.5rem;
`

export const ProfilePosition = style.small`
  display: block;
  font-size: 1.2rem;
  font-weight: 300;
`
export const ProfileDescription = style.p`
  font-size: 1rem;
  font-weight: 300;
  line-height: 1.4;
`
