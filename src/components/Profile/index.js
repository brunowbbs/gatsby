import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import Avatar from "../Avatar/index"
import * as S from "./styled"

const Profile = () => {
  const resultado = useStaticQuery(graphql`
    {
      site {
        siteMetadata {
          title
          position
          description
          author
        }
      }
    }
  `)

  return (
    <S.ProfileWrapper>
      <S.ProfileLink>
        <Avatar />
        <S.ProfileAuthor>
          {resultado.site.siteMetadata.title}
          <S.ProfilePosition>
            {resultado.site.siteMetadata.position}
          </S.ProfilePosition>
        </S.ProfileAuthor>
      </S.ProfileLink>
      <S.ProfileDescription>
        {resultado.site.siteMetadata.description}
      </S.ProfileDescription>
    </S.ProfileWrapper>
  )
}

export default Profile
