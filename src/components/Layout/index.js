import React from "react"

import * as S from "./styled"
import Sidebar from "../Sidebar"
import MenuBar from "../MenuBar"

import GlobalStyle from "../../styles/Global"

const Layout = ({ children }) => {
  return (
    <S.LayoutWrapper>
      <GlobalStyle />
      <Sidebar />
      <S.LayoutMain>{children}</S.LayoutMain>
      <MenuBar />
    </S.LayoutWrapper>
  )
}

export default Layout
