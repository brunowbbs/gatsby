import style from "styled-components"

export const SidebarWrapper = style.aside`
  align-items:center;
  border-right: 1px solid #38444d;
  background-color: #192734;
  display: flex;
  flex-direction: column;
  height:100vh;
  position: fixed;
  padding: 2rem;
  text-align: center;
  width:20rem
`
